# Pythagorean Theorem Calculator in C
The Pythagorean theorem is named after Pythagoras, a Greek mathematician who lived around 500 BC. The theorem is a formula that connects the areas of squares that can be drawn on the triangle’s sides for any right-angled triangle.

### Area of square C = area of square A + area of square B
`c^2 = a^2 + b^2`
In other words, “the square on the hypotenuse of a right-angled triangle is equal to the sum of the squares of the other two sides.” Pythagoras’ theorem is used to determine the length of one side of a right-angled triangle when the lengths of the other two sides are known.


### Solving Example on Pythagorean Theorem using C ;
#### Example :
A “flying fox” is created by stretching a wire between two upright poles. The distance between the poles is 13 metres. Find the height of the larger pole if the shorter pole’s height is 6 metres and the wire’s length is 15 metres. Assume the wire is straight and has no “sag.”
![alt text](https://calculatorhub.org/wp-content/uploads/2022/01/%E2%96%B3h-2.png)
#### Solution :
We need a right-angled triangle to employ Pythagoras’ theorem, which is formed by drawing a horizontal line from the top of the shorter pole to the top of the longer pole. Let x meters be the unknown length of the triangle. This triangle can now be solved using Pythagoras’ theorem.

 15^2 = x2 + 13^2  Pythagoras’ theorem
 225 = x2 + 169
225 – 169 = x2
x2 = 56
x = 7.4833

### Solution in code :
![alt text](https://xp.io/storage/2EHkQri1.png)


