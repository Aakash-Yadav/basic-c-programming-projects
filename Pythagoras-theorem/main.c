#include <stdio.h>
#include <math.h>


int main() {
  // a^2 + b^2 = c^2
  unsigned int A,B,C;
  float result;

  printf("Enter 0 for hypothesis \n\n");

  printf("Enter The value of A ");
  scanf("%d", & A);

  printf("Enter the value of B ");
  scanf("%d", & B);

  printf("Enter the value of C ");
  scanf("%d", & C);

  if (!C) {

    result = (A * A) + (B * B);

  } else if (!B) {

    result = (C * C) - (A * A);

  } else {

    result = (C * C) - (B * B);

  }

  printf("\nThe value of hypothesis = %0.4f \n", sqrt(result));

  return 0;

}
