
#include <stdio.h>

int GCD(int num1, int num2){
   if (num2==0){
        return num1 ;
    }
    else{
        return GCD(num2,num1%num2);
    }
}

int Lcm(int num1,int num2){
  int value = GCD(num1,num2);
  return (num1*num2)/GCD(num1,num2);
 }


int main(){
  int num1,num2; 
  printf("Enter the number 1 : ");
  scanf("%d",&num1);
  printf("Enter the number 2 : ");
  scanf("%d",&num2);
  printf("\nThe Greatest Common Denominator of %d and %d = %d \n",num1 , num2 ,GCD(num1,num2));
  printf("The Least Common Multiple of %d and %d = %d \n\n",num1,num2,Lcm(num1,num2));
  return 0;
}
