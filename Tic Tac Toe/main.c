#include <stdio.h>

void print_table(int *n)
{
    for (int i = 0; i < 9; i++)
    {
        if ((i + 1) % 3 == 0)
        {
            printf("%d  \n", *n);
        }
        else
        {
            printf("%d  ", *n);
        }
        n = n + 1;
    }
}

int winner(int w[])
{
    if ((w[0] + w[4] + w[8]) == 3 || (w[2] + w[4] + w[6]) == 3 || (w[0] + w[4] + w[8]) == -3 || (w[2] + w[4] + w[6]) == -3)
    {
        return 1;
    }
    else if ((w[0] + w[1] + w[2]) == 3 || (w[3] + w[4] + w[5]) == 3 || (w[6] + w[7] + w[8]) == 3 ||
             (w[0] + w[1] + w[2]) == -3 || (w[3] + w[4] + w[5]) == -3 || (w[6] + w[7] + w[8]) == -3)
    {
        return 1;
    }
    else if ((w[0] + w[3] + w[6] == 3) || (w[1] + w[4] + w[7] == 3) || (w[2] + w[5] + w[8] == 3) || (w[0] + w[3] + w[6] == -3) || (w[1] + w[4] + w[7] == -3) || (w[2] + w[5] + w[8] == -3))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void game(int value[9], int *val, int pre)
{

    int play = 9;
    int turn_ = 1;
    int add_value;
    play = play - pre;
    do
    {
        printf("\n");
        print_table(val);
        printf("Enter The position of player %d? ", turn_);
        scanf("%d", &add_value);
        add_value = add_value - 1;
        if (value[add_value] == 0 && turn_)
        {
            value[add_value] = 1;
            turn_ = 0;
        }
        else if (value[add_value] == 0 && !turn_)
        {
            value[add_value] = -1;
            turn_ = 1;
        }
        else
        {
            return game(value, val, pre);
        }
        if (winner(value))
        {
            if (turn_)
            {
                printf(" \nPlayer %d winn \n", turn_ - 1);
                break;
            }
            printf(" \nPlayer %d winn \n", turn_ + 1);
            break;
        }
        play -= 1;
    } while (play >= 0);
}

int main()
{
    int value[9];
    for (int i = 0; i < 9; i++)
    {
        value[i] = 0;
    }
    game(value, &value, 0);
}
