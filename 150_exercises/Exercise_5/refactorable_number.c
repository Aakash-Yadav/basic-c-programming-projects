#include <stdio.h>
#include<stdlib.h>

int return_count_factors(int number){
    int i = 1;
    int count = 0;
    while(i<=number){
        if(number%i==0){
          count++;  
        }
        i++;
    }return count;
}

int Refactor_number(int n){
    int count = return_count_factors(n);
    return n%count;
}

int main(){
    for(int i=2+1;i<1000;i++){
        if(Refactor_number(i)==0){
            printf("%d \n",i);
        }
    }
}
