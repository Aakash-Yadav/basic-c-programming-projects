
#include <stdio.h>

void days_into_years_weeks(int day){
    float year = (float)(day/365);
    float week = (float)((day%365)/7);
    printf("Years: %.3f\n", year);
    printf("Weeks: %.3f\n", week);
}

int main(){
    int Day;
    scanf("%d",&Day);
    days_into_years_weeks(Day);
}
