#include <stdio.h>

void reverse(char point_value[], int size) {
   char temp;
   for (int i = 0; i < (int)(size / 2); i++) {
      temp = point_value[i];
      point_value[i] = point_value[size - i - 1];
      point_value[size - i - 1] = temp;
   }
   for (int j = 0; j < size; j++) {
      printf("%c", point_value[j]);
   }
}

int main() {
   char word[3] = {'X','M','L'};
   char test[] = "Akash-Yadav";
   reverse(word, sizeof(word) / sizeof(word[0]));
   printf("\n");
   reverse(test, sizeof(test) / sizeof(test[0]));
   return 0;
}
