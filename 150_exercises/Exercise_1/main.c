#include <stdio.h>

void f_block(int size) {

  int mid = (int)(size / 2);

  for (int i = 0; i < size; i++) {
    if (i == mid || i == 0) {
      for (int j = 0; j < 5; j++) {
        printf("#");
      }
      printf("\n");
    } else {
      printf("#\n");
    }
  }
  printf("\n");

}

void c_block(int size) {
  int iter = 0;
  while (iter != size + 1) {
    if (!iter || iter == size) {
      int j = 0;
      do {
        printf("#");
        j++;
      } while (j != 10);
      printf("\n");
    } else {
      printf("#\n");
    }
    iter++;
  }
}

int main() {

  int F = 7, C = 9;
  printf("F Block \n\n");
  f_block(F);
  printf("C Block \n");
  printf("\n");
  c_block(C);

  return 0;
}
