#include <stdio.h>
#include <math.h>

int Area(int a, int b) {
   return a * b;
}

float Diagonal(int a, int b) {
   float value = sqrt(pow(a, 2) + pow(b, 2));
   return value;
}

int Perimeter(int a, int b) {
   return 2 * (a + b);
}

int main() {

  int length, width;
   printf("Enetr The length of Rectangle \n");
   scanf("%d", & length);
   printf("Enetr The width of Rectangle \n");
   scanf("%d", & width);

   printf("The Area of Rectangle = %d\n", Area(length, width));
   printf("The Diagonal of Rectangle = %.3f\n", Diagonal(length, width));

   printf("The Perimeter of Rectangle = %d\n", Perimeter(length, width));

}
