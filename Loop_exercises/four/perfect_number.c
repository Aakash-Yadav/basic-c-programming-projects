//Write a c program to check whether a given number is a perfect number or not.

#include <stdio.h>

int given_number_is_a_perfect(int n){
    int i=1;
    int sum=0;
    while(i<n){
        if(n%i==0){
            sum+=i;
        }
        i++;
    }
    return sum==n;
}

int main(){
    printf("The list of perfect number 0-1000 \n");
    for(int i=1;i<=1000;i++){
       if(given_number_is_a_perfect(i)){
           printf("%d ",i);
       }else{
           continue;
       }
    }
    return 0;
}
