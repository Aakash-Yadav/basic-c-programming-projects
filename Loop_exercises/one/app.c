#include <stdio.h>

//. Write a program in C to display the multiplication table vertically from 1 to n.

void multiplication_table(int *given_number){
    int i=1;
  while(i!=10){
    printf("%d x %d = %d \n",*given_number,i,(i**given_number));
    i++;
  }
}

void display_to_n_multiplication_table(int *num){
    for(int i=1;i<(*num);i++){
      printf("\n");
      multiplication_table(&i);
    }
}

int main(){
    int user_value;
    scanf("%d",&user_value);
    printf("The multiplication table of %d \n",user_value);
    // multiplication_table(&user_value);
    display_to_n_multiplication_table(&user_value);
}
