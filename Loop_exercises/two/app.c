
// Write a program in C to display the n terms of harmonic series and their sum
#include <stdio.h>

double display_harmonic_series(int display_length){
    int i=1;
    float sum;
    while(i!=(display_length+1)){
        sum+= (1/(float)i);
        i++;
    }
    return sum;
}

int main(void){
    int length = 5;
    float value = display_harmonic_series(length);
    printf("Sum of Series upto %d terms : %.3f \n",length,value);
    return 0;
}

