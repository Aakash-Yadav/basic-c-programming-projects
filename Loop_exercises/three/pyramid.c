//  Write a program in C to display the pattern like a pyramid using asterisk and each row contain an odd number of asterisks. 

#include <stdio.h>

void display_the_pattern(int value){
    int i=0;
    int space=value+4-1;
    while(i!=value){
        int k = space;
        do{
            printf(" ");
            k--;
        }while(k>=1);
        for(int j=0;j<((2*i)+1);j++){
            printf("*");
        }
        printf("\n");
        i++;
        space--;
    }    
}

int main(){
    display_the_pattern(3+4);
    return 0;
}
