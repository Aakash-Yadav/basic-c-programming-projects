#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct vector{
    int x;
    int y;
}vec;

void dec_random_int(vec *ptr){
    ptr->x = rand() % 99 +1;
    ptr->y = rand() % 99 +1;
   print_ptr(ptr);
}

void print_ptr(struct vector *ptr){
    printf("x = %d y = %d \n",ptr->x,ptr->y);
}

struct vector sum_(struct vector *ptr,struct vector *ptr_2){
    vec sum;
    sum.x = ptr->x+ptr_2->x;
    sum.y = ptr->y+ptr_2->y;
    return sum;
}

int main(){
    srand(time(0));
    vec one,two,sum;
    vec *pt1 = &one;
    vec *pt2 = &two;
    dec_random_int(pt1);
    dec_random_int(pt2);
    sum = sum_(pt1,pt2);
     printf("RESULT \n");
    print_ptr(&sum);
    
    return 0;
    
}
