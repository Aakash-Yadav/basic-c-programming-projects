#include <stdio.h>

int power(int base, int expo){
    int result = 1;
    while(expo) {   
        result = result * base; 
        expo--; 
    }
    return result;
}

int isPerfectSquare(int x){
    int left = 1, right = x;
    int out = 0;
    while (left <= right) {
        int mid = (left + right) / 2;
        if (mid * mid == x) {
            out = mid;
            break;
        }
        if (mid * mid < x) {
            left = mid + 1;
        }else {
            right = mid - 1;
        }
    }
    return out;
}

int distance_of_perfect_squares(int num,int way){
    int result = num;
    // printf("%d",isInteger(num) && isInteger(num*num));
    while(1){
        // printf("%d \n",isInteger(num));
        if(isPerfectSquare(num)){
            break;
        }else{
            if(way){
                result+=1;
                num++;
            }else{
                result-=1;
                num--;
            }
        }
    }
    return result;
}

float main_calculation(int low_distance_perfect_square,int num){
    printf("Given number = %d  , closest perfect square = %d \n",num,low_distance_perfect_square);
    return (float)(num+low_distance_perfect_square)/(2*isPerfectSquare(low_distance_perfect_square));
}

float main_fun_to_find_square_root(int num){
    if(isPerfectSquare(num)){
        return isPerfectSquare(num);
    }else{
        float out_result;
        // int low,high
         int low = distance_of_perfect_squares(num,0);
         int high = distance_of_perfect_squares(num,1);
        if((num-low)>(num-high)){
            out_result = main_calculation(high,num);
        }else{
            out_result = main_calculation(low,num);
        }
         return out_result;
    }
}

int main(void){
    printf("the square root of 11 = %.4f \n",
    main_fun_to_find_square_root(55));
}
