
#include <stdio.h>

int main(){
    float percentage;
    int math,physic,chem;
    
    printf("Enter The value of maths \n");
    scanf("%d",&math);
    
    printf("Enter The value of physics \n");
    scanf("%d",&physic);
    
    printf("Enter The value of chemistry \n");
    scanf("%d",&chem);
    
    percentage = ((float)(math+physic+chem)/300)*100;
    
    printf("Thre percent is %.3f \n",percentage);
    
    if(percentage>90 && percentage<100){
        printf("A+");
    }
    else if(percentage<90 && percentage>80){
        printf("GRADE = A \n");
    }else if(percentage>70 && percentage<80){
        printf("GRADE = B \n");
    }else if(percentage>60 && percentage<70){
        printf("GRADE = C \n");
    }else if(percentage>60 && percentage<50){
        printf("GRADE = D \n");
    }else{
        printf("Fail \n");
    }
}
