
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
    srand(time(0));
    int random_number = rand()%100+1;
    unsigned int guess,attempt=0;
    
    do{
        printf("Guess the number between 1-100 \n");
        scanf("%d",&guess);
        
        if(guess>random_number){
            printf("Lower Number Please \n\n");
        }else if(guess<random_number){
            printf("Higher Number Please \n\n");
        }else{
            printf("Guess right %d,%d \n",random_number,guess);
            printf("Total attemp = %d \n",attempt);
        }
        attempt++;
    }while(guess!=random_number);
    
    return 0;
}

