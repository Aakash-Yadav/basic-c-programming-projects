#include <stdio.h>

int fibbonacci(int n) {
   if(n == 0){
      return 0;
   } else if(n == 1) {
      return 1;
   } else {
      return (fibbonacci(n-1) + fibbonacci(n-2));
   }
}

int factorial(int number){
    if(number==0){
        return 1;
    }else{
        return number*factorial(number-1);
    }
}

int main(){
    printf("Enter The number : \n");
    
    int value;scanf("%d",&value);

    printf("The factorial of number %d = %d \n",value,factorial(value));
    
    printf("The factorial numbers 0-%d are \n",value);
    
    for(int v=0;v<value;v++){
        printf("%d \n",fibbonacci(v));
    }
    
    
}
