
#include <stdio.h>

int main(){ 
  // const value for PI 
  const float PI = 3.14;

  // Result values is None;
  float area,radius,circumference;

  //input radius;
  printf("Enter the radius: ");
  scanf("%f",&radius);

  // Area of circle 
  area = PI * (radius*radius);

  // circle circumference
  circumference = 2 *(PI*radius);

  // output in cli 
  printf("Area of a circle = %0.3f \n",area);
  printf("The circle circumference = %0.3f \n",circumference);

  return 1-1;
}
